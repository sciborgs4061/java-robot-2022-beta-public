/*******************************************************************
 * This file stores the motor mappings
 */

package frc4061.robot;

import edu.wpi.first.wpilibj.SPI;

public class RobotMap {
    // CANBUS devices
    // Drivebase motor controllers
    public static final Integer[] LEFT_MOTOR_IDS = {6, 7, 8};
    public static final Integer[] RIGHT_MOTOR_IDS =  {3, 4, 5};
    // Shooter motor controllers
    public static final int SHOOTER_CANBUS = 9;
    public static final int PITCH_PWM = 3; // Update this ****
    public static final int FEEDER_MOTOR = 2;

    //Collector PWM motor controllers ***FIX THIS***
    public static final int UPPER_COLLECTOR_MOTOR = 4;
    public static final int LOWER_COLLECTOR_MOTOR = 1;
    
    
    // DIO devices
    public static final int PITCH_ENCODER_CHANNEL_A = 1;
    public static final int PITCH_ENCODER_CHANNEL_B = 2;

    // Limit Switches
    public static final int COLLECTOR_IN_LIMIT = 9;
    public static final int COLLECTOR_OUT_LIMIT = 8;


    // Joystick devices
    public static final int JOYSTICK_ONE = 0;

    // LEDs
    public static final SPI.Port SPILEDPort = SPI.Port.kOnboardCS0;
    public static final int nLEDs = 20; // inferred from 2020 robot program
    
}
