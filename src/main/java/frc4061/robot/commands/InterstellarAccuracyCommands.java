package frc4061.robot.commands;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc4061.robot.Constants;
import frc4061.robot.OperatorInterface;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.utilities.InchUtils;

// These commands just drive straight for the appropriate distance
// adjust the shooter angle
// then shoot 3 balls

public class InterstellarAccuracyCommands {

    public InterstellarAccuracyCommands(Drivebase drivebase, FollowTrajectoryFactory factory) {
        m_drivebase = drivebase;
        m_followerFactory = factory;
    }

    public Command buildIAProgram () {
        // Pause for driver/operator input before each step
        // add shooter aiming, shooting, loading, etc.
        // as they become available
        Command iaPgm = new InstantCommand(() -> m_drivebase.resetOdometry(inchPose(startPoint)))
           .andThen(trig(driveToGreenShot()))
           .andThen(trig(afterGreenShot()))
           .andThen(trig(driveToYellowShot()))
           .andThen(trig(afterYellowShot()))
           .andThen(trig(driveToBlueShot()))
           .andThen(trig(afterBlueShot()))
           .andThen(trig(driveToRedShot()))
           .andThen(trig(afterRedShot()))
           .andThen(trig(driveToBlueShot()))
           .withName("Interstellar Accuracy")
           ;
        return iaPgm;
      }
    
    private final Command driveToGreenShot() { return driveFromTo(startPoint, greenShotPoint, false); }
    private final Command afterGreenShot() { return driveFromTo(greenShotPoint, reloadPoint, true); }
    private final Command driveToYellowShot() { return driveFromTo(reloadPoint, yellowShotPoint, false); }
    private final Command afterYellowShot() { return driveFromTo(yellowShotPoint, reloadPoint, true); }
    private final Command driveToBlueShot() { return driveFromTo(reloadPoint, blueShotPoint, false); }  
    private final Command afterBlueShot() { return driveFromTo(blueShotPoint, reloadPoint, true); } 
    private final Command driveToRedShot() { return driveFromTo(reloadPoint, redShotPoint, false); }
    private final Command afterRedShot() { return driveFromTo(redShotPoint, reloadPoint, true); }
    private final Command aimGreen() { return null; } // these all need to be defined, of course.
    private final Command aimBlue() { return null; }
    private final Command aimYellow() { return null; }
    private final Command aimRed() { return null; }
    private final Command shoot() { return null; }
    private final Command intake() { return null; }
    
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_followerFactory;

    // these x coordinates are where the front bumper of the robot should be for each shot
    private static final double halfRL = Constants.Drivebase.kRobotLengthInches/2;
    private static final double startPoint = 90-halfRL; // inches from plane of the target
    private static final double greenShotPoint = 80-halfRL; // a little drive ahead to fine tune aim
    private static final double yellowShotPoint = 90+halfRL;
    private static final double blueShotPoint = 150+halfRL;
    private static final double redShotPoint = 210+halfRL;
    private static final double reloadPoint = 360-halfRL; // back wall - robot length
    // all driving in this skill task takes place on the center line
    private static final double centerLine = 90;
    private static final double driveAngleDeg = 180;

    private static final Pose2d inchPose(double inchX) {
        return InchUtils.pose2d( inchX, centerLine, driveAngleDeg);
    }
    
    private final Command driveFromTo(double startLine, double finishLine, boolean reversed) {
        Pose2d startPose = inchPose(startLine);
        Pose2d finishPose = inchPose(finishLine);
        Spline line = new LinearPseudoSpline(startPose.getTranslation(), finishPose.getTranslation());
        Trajectory traj = HookTrajectories.trajectoryFromSplines(new Spline[] {line},
                                                                 reversed ? Constants.Drivebase.configReversed : Constants.Drivebase.configForward);
        Command cmd = m_followerFactory.getFollower(traj);
        Command ret = cmd.beforeStarting(() -> m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters()));
        return ret;
    }

    private Command waitWhileStoppedCommand() { 
        // Need to feed the motors while waiting!!
        // The RunCommand feeds the motors until the wait condition
        // is satisfied, then the cmd is run
        return 
            new WaitUntilCommand(() -> OperatorInterface.getProceed())
                .raceWith(new RunCommand(() -> m_drivebase.stop()));
    }

    private Command trig(Command cmd) {

        return waitWhileStoppedCommand().andThen(cmd);
    }

}
