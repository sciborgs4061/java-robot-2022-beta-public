package frc4061.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.OperatorInterface;
import frc4061.robot.subsystems.Drivebase;

public class DriveNormal extends CommandBase {

    private static final String speed = "Speed";
    private static final String rightTrigger = "Trigger right";

    private final Drivebase m_drivebase;

    public DriveNormal(Drivebase drivebase) {
        m_drivebase = drivebase;  
        addRequirements(drivebase);

        SmartDashboard.putNumber(speed, 0);
        SmartDashboard.putNumber(rightTrigger, 0);

    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        m_drivebase.curvatureDrive(OperatorInterface.getSpeed(), OperatorInterface.getTurn());
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        m_drivebase.arcadeDrive(0.0, 0.0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}
