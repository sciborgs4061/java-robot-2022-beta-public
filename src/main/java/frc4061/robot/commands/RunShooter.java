package frc4061.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.OperatorInterface;
import frc4061.robot.subsystems.FalconShooter;

public class RunShooter extends CommandBase {
    private static final String upperSetpointRPMkey = "Setpoints/RPM: Upper Shooter";
    private static final String upperSetpointsFractionkey = "Setpoints/Fraction [-1,1]: Upper Shooter";
    private static final String upperRPMSensor = "Sensors/Upper Shooter RPM";
    private static final String upperOutputSensor = "Sensors/Upper Shooter Output";
    private static final String upperInputSensor = "Sensors/Upper Shooter Input";
    private static final String encoderCount = "Encoder";
    private static final String isSped = "Up to Speed";

    private static final String shooterGreen = "Angle/Green Zone";
    private static final String shooterYellow = "Angle/Yellow Zone";
    private static final String shooterBlue = "Angle/Blue Zone";
    private static final String shooterRed = "Angle/Red Zone";
    private static final String shooterPurple = "Angle/Purple Zone";

    private FalconShooter m_shooter;

    private SendableChooser<String> zoneChooser;

    public RunShooter(FalconShooter shooter) {
        m_shooter = shooter;
        addRequirements(shooter);
        double upperRPM = SmartDashboard.getNumber(upperSetpointRPMkey, 4500);
        SmartDashboard.putNumber(upperSetpointRPMkey, upperRPM);
        double upperOutput = SmartDashboard.getNumber(upperSetpointsFractionkey, 0);
        SmartDashboard.putNumber(upperSetpointsFractionkey, upperOutput);
        SmartDashboard.putNumber(upperRPMSensor, 0);
        SmartDashboard.putNumber(upperOutputSensor, 0);
        SmartDashboard.putNumber(upperInputSensor, 0);
        SmartDashboard.putBoolean(isSped, false);
        SmartDashboard.putNumber(encoderCount, 10000);

        zoneChooser = new SendableChooser<>();
        zoneChooser.setDefaultOption("Green", "Green");
        zoneChooser.addOption("Green", "Green");
        zoneChooser.addOption("Yellow", "Yellow");
        zoneChooser.addOption("Blue", "Blue");
        zoneChooser.addOption("Red", "Red");
        zoneChooser.addOption("Purple", "Purple");

        SmartDashboard.putData("Zone", zoneChooser);

        SmartDashboard.putNumber(shooterGreen, 0);
        SmartDashboard.putNumber(shooterYellow, 1250);
        SmartDashboard.putNumber(shooterBlue, 2400);
        SmartDashboard.putNumber(shooterRed, 3000);
        SmartDashboard.putNumber(shooterPurple, 3200);
    }

    @Override
    public void initialize() {
        // m_shooter.zeroShooter();
    }
    @Override
    public void execute() {
        // get setpoints from Smart Dashboard   
        double upperRPM = SmartDashboard.getNumber(upperSetpointRPMkey, 0);
        double upperOutput = SmartDashboard.getNumber(upperSetpointsFractionkey, 0);

        if ((upperRPM != 0)) {  
            m_shooter.setSpeeds(upperRPM);
        } else {
            m_shooter.setOutputs(upperOutput);
        }

        //m_shooter.autoLiftShooter(selectActiveZone());
        //m_shooter.zeroShooter();
        moveShooter();
        //m_shooter.moveShooter(selectActiveZone());
        m_shooter.toggleLift(selectActiveZone());
        m_shooter.initializeEncoder();
        //m_shooter.liftShooter();
        // set RPM output on the Smart Dashboard
        Double rpm = m_shooter.getSpeed();
        Double stator = m_shooter.getStator();
        Double supply = m_shooter.getSupply();
        Double encoder = m_shooter.getEncoder();
        boolean spedUp = m_shooter.upToSpeed();
        
        SmartDashboard.putNumber(encoderCount, encoder);
        SmartDashboard.putNumber(upperRPMSensor, rpm);
        SmartDashboard.putNumber(upperOutputSensor, stator);
        SmartDashboard.putNumber(upperInputSensor, supply);
        SmartDashboard.putBoolean(isSped, spedUp);
        m_shooter.update();
    }

    public double selectActiveZone(){
        String activeZone = zoneChooser.getSelected();

        switch(activeZone) {
            case "Green":
                return SmartDashboard.getNumber(shooterGreen, 0);
            case "Yellow":
                return SmartDashboard.getNumber(shooterYellow, 1250);
            case "Blue":
                return SmartDashboard.getNumber(shooterBlue, 2400);
            case "Red":
                return SmartDashboard.getNumber(shooterRed, 3000);
            case "Purple":
                return SmartDashboard.getNumber(shooterRed, 3200);
            default:
                return 0;
        }
    }

    public void moveShooter() {
        if (OperatorInterface.decrementShooterByTen())
            m_shooter.moveShooter(m_shooter.getEncoder() - 69);
        else if (OperatorInterface.incrementShooterByTen())
            m_shooter.moveShooter(m_shooter.getEncoder() + 69);
        else
            return;
    }

    @Override
    public boolean isFinished() {return false;}

    @Override
    public void end(boolean interrupted) {
        m_shooter.stopShooter();
    }  
}
