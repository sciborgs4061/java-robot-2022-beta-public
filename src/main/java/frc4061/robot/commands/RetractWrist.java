package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Intake;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RetractWrist extends CommandBase{
    private Intake m_intake;

    private static final String openLimit = "Open limit switch";
    private static final String closeLimit = "Close limit switch";

    public RetractWrist(Intake intake) {
        m_intake = intake;
        addRequirements(intake);
        SmartDashboard.putBoolean(openLimit, false);
        SmartDashboard.putBoolean(closeLimit, false);
    }

    @Override
    public void initialize(){}

    @Override
    public void execute() {
        m_intake.retractCollector();

        SmartDashboard.putBoolean(openLimit, m_intake.retracted());
        SmartDashboard.putBoolean(closeLimit, m_intake.extended());
    }

    @Override
    public boolean isFinished() {return false;}

    @Override
    public void end(boolean interrupted)
    {
        m_intake.stopIntake();
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}
