package frc4061.robot.commands;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.Command;
import frc4061.robot.Constants;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.utilities.InchUtils;

// These commands just drive straight for the appropriate distance
// adjust the shooter angle
// then shoot 3 balls

public class Forward100 {

    public Forward100(Drivebase drivebase, FollowTrajectoryFactory factory) {
        m_drivebase = drivebase;
        m_followerFactory = factory;
    }

    public Command forward100 () {
        // Pause for driver/operator input before each step
        // add shooter aiming, shooting, loading, etc.
        // as they become available
        Command pgm = driveFromTo(70, 203, false);
        return pgm;
      }
    
    
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_followerFactory;

    private static final Pose2d inchPose(double inchX) {
        return InchUtils.pose2d( inchX, 90, 0);
    }
    
    private final Command driveFromTo(double startLine, double finishLine, boolean reversed) {
        Pose2d startPose = inchPose(startLine);
        Pose2d finishPose = inchPose(finishLine);
        Spline line = new LinearPseudoSpline(startPose.getTranslation(), finishPose.getTranslation());
        Trajectory traj = HookTrajectories.trajectoryFromSplines(new Spline[] {line},
                                                                 reversed ? Constants.Drivebase.configReversed : Constants.Drivebase.configForward);
        Command cmd = m_followerFactory.getFollower(traj);
        Command ret = cmd.beforeStarting(() -> m_drivebase.resetOdometry(inchPose(startLine)));
        return ret;
    }

}
