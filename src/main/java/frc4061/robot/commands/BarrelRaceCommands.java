package frc4061.robot.commands;

import frc4061.robot.Constants;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.utilities.InchUtils;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;

public class BarrelRaceCommands {

    public BarrelRaceCommands(Drivebase drivebase, FollowTrajectoryFactory factory) {
        m_drivebase = drivebase;
        m_factory = factory;
    }
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_factory;

    public Command buildBarrelRaceProgram () {
        Pose2d startPose = new Pose2d(Units.inchesToMeters(30), Units.inchesToMeters(82), new Rotation2d()); 
        
        Spline[] spline = {
            new LinearPseudoSpline(InchUtils.translation2d(30.0, 96.0), InchUtils.translation2d(145.5, 90.5)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(145.5, 90.5, -2.8), -30.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(157.4, 89.1, -14.3), 30.0, -334.2),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(156.0, 90.2,  0.0), -30.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(156.0, 90.2), InchUtils.translation2d(234.0, 89.8)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(234.0, 89.8, -0.0), 30.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(246.0, 90.6, 11.5), 30.0, 292.1),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(222.9, 94.4, -45.0), 30.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(222.9, 94.4), InchUtils.translation2d(274.4, 42.9)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(274.4, 42.9, -45.0), 30.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(283.4, 35.0, -33.5), 30.0, 200.8),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(294.7, 90.3, 178.7), 30.0, 12.0, false),
            // new LinearPseudoSpline(InchUtils.translation2d(294.7, 90.3), InchUtils.translation2d(30.0, 96.0)),

                                   
        };
        Trajectory traj = HookTrajectories.trajectoryFromSplines(spline, Constants.Drivebase.configForwardNoStop);
        Spline[] runHome = {new LinearPseudoSpline(InchUtils.translation2d(294.7, 104.3), InchUtils.translation2d(30.0, 96.0)),};

        Trajectory runHomeTraj = HookTrajectories.trajectoryFromSplines(runHome, Constants.Drivebase.configFastForward);
        Command cmd1 = m_factory.getFollower(traj);
        Command cmd2 = m_factory.getFollower(runHomeTraj);
        Command cmd = new InstantCommand(() -> SmartDashboard.putNumber("Auto Timing/Start", Timer.getFPGATimestamp()))
                                    .andThen(() -> m_drivebase.resetOdometry(startPose))
                                    .andThen(cmd1)
                                    .andThen(()-> m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters()))
                                    .andThen(cmd2)
                                    .andThen(() -> SmartDashboard.putNumber("Auto Timing/Stop", Timer.getFPGATimestamp()));
        System.out.println("Barrel Race predicted time: " + traj.getTotalTimeSeconds() + runHomeTraj.getTotalTimeSeconds());
        return cmd;
    } 
}

