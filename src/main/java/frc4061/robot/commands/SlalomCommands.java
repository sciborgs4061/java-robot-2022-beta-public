package frc4061.robot.commands;

import frc4061.robot.Constants;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.utilities.InchUtils;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.Command;

public class SlalomCommands {

    public SlalomCommands(Drivebase drivebase, FollowTrajectoryFactory factory) {
        m_drivebase = drivebase;
        m_factory = factory;
    }
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_factory;

    public Command buildSlalomProgram () {
        Pose2d startPose = InchUtils.pose2d(30, 30, 0); 
        Spline[] spline = {
            new LinearPseudoSpline(InchUtils.translation2d(45.0, 30.0), InchUtils.translation2d(55.9, 30.5)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(55.9, 30.5,  3.6), 29.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(67.7, 32.0, 15.5), 29.0, 47.9),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(89.8, 58.3, 75.2), 29.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(89.8, 58.3), InchUtils.translation2d(90.2, 61.7)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(90.2, 61.7, 75.2), -29.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(94.1, 73.0, 63.3), 29.0, -51.5),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(126.0, 89.2,  0.0), -29.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(126.0, 89.2), InchUtils.translation2d(234.0, 89.2)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(234.0, 89.2,  0.0), -29.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(246.0, 88.4, -11.9), 29.0, -51.5),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(269.8, 61.7, -75.2), -29.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(269.8, 61.7), InchUtils.translation2d(270.2, 58.3)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(270.2, 58.3, -75.2), 29.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(274.1, 47.0, -63.3), 29.0, 306.6),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(270.2, 61.7, -104.8), 29.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(270.2, 61.7), InchUtils.translation2d(269.8, 58.3)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(269.8, 58.3, -104.8), -29.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(265.9, 47.0, -116.7), 29.0, -52.4),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(233.5, 30.9, 179.0), -29.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(233.5, 30.9), InchUtils.translation2d(125.5, 32.7)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(125.5, 32.7, 179.0), -27.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(113.6, 33.8, 166.3), 27.0, -42.5),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(92.4, 55.8, 111.0), -27.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(92.4, 55.8), InchUtils.translation2d(89.4, 64.9)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(89.4, 64.9, 111.0), 29.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(84.4, 75.7, 122.9), 29.0, 31.1),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(61.3, 89.8, 165.8), 29.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(61.3, 89.8), InchUtils.translation2d(20.0, 100.0)),
           
        };
        Trajectory traj = HookTrajectories.trajectoryFromSplines(spline, Constants.Drivebase.configForward);
        Command cmd = m_factory.getFollower(traj).beforeStarting(() -> m_drivebase.resetOdometry(startPose))
            .beforeStarting(() -> SmartDashboard.putNumber("Auto Timing/Start", Timer.getFPGATimestamp()))
            .andThen(() -> SmartDashboard.putNumber("Auto Timing/Stop", Timer.getFPGATimestamp()));
        System.out.println("Slalom predicted time: " + traj.getTotalTimeSeconds());
        return cmd;
    }   
}