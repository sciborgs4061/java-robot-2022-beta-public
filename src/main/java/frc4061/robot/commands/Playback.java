package frc4061.robot.commands;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Drivebase;

public class Playback extends CommandBase {
    private final Drivebase m_drivebase;
    private int m_counter;
    private int m_nEntries;
    private double m_data[][];
    static final int maxEntries = 10000;

    public Playback(Drivebase subsystem) {

        m_drivebase = subsystem;
        addRequirements(m_drivebase);
        m_data = new double[maxEntries][2];

    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        m_counter = 0;
        m_nEntries = 0;  
        DataInputStream is;
        try {
            is = new DataInputStream(new FileInputStream("/home/lvuser/BlackBoxData-playback"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("Playback file not found: BlackBoxData-playback");
            is = null;
        }
        if (is != null) try {
            while(true) {
                if (m_nEntries < maxEntries) {
                    m_data[m_nEntries][0] = is.readDouble();
                    m_data[m_nEntries][1] = is.readDouble();
                    m_nEntries++;
                } else {
                    break;
                }                
            } 
        } catch (IOException e) {
            // this should just be end-of-file
        }
    }
   
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        if (m_counter < m_nEntries) {
            m_drivebase.tankDriveVolts(m_data[m_counter][0], m_data[m_counter][1]);
            m_counter++;
        }
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        m_drivebase.stop();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return m_counter >= m_nEntries;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;

    }
}

