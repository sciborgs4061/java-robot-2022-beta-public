package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Feeder;
import frc4061.robot.OperatorInterface;


public class Feed extends CommandBase {
    private final Feeder m_feeder;

    public Feed(Feeder feeder) {
        m_feeder = feeder;
        addRequirements(feeder);
    }

    @Override
    public void execute() {
        m_feeder.set_speed(OperatorInterface.getFeeding());
    }

    @Override
    public void end(boolean interrupted) {
        m_feeder.set_speed(0);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}