package frc4061.robot.commands;

import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.Constants;

public class FollowTrajectoryFactory {

    private final Drivebase m_drivebase;

    public FollowTrajectoryFactory(Drivebase drivebase) {

        m_drivebase = drivebase;
        // other stuff common to all trajectory-followers on this drivebase

    }

    public Command getFollower (Trajectory trajectory) {
        RamseteCommand ramseteCommand = new RamseteCommand (
            trajectory,
            m_drivebase.m_odometry::getPoseMeters,
            new RamseteController(Constants.Drivebase.kRamseteB, Constants.Drivebase.kRamseteZeta),
            new SimpleMotorFeedforward(Constants.Drivebase.ksVolts,
                                       Constants.Drivebase.kvVoltSecondsPerMeter,
                                       Constants.Drivebase.kaVoltSecondsSquaredPerMeter),
            Constants.Drivebase.kDriveKinematics,
            m_drivebase::getWheelSpeeds,
            new PIDController(Constants.Drivebase.kPDriveVel, 0, 0),
            new PIDController(Constants.Drivebase.kPDriveVel, 0, 0),
            m_drivebase::tankDriveVolts,
            m_drivebase
        );
        /*
        return new CommandsWithFinally(ramseteCommand.
                                       beforeStarting(() -> m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters())))
                                       //.andFinally(() -> m_drivebase.stop())
                                       ;
        */
        ramseteCommand.addRequirements(m_drivebase);
        return ramseteCommand.beforeStarting(() -> 
                   m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters()));
    }
}
