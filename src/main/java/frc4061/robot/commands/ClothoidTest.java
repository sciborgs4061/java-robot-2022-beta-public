package frc4061.robot.commands;

import frc4061.robot.Constants;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;

public class ClothoidTest {

    public ClothoidTest(Drivebase drivebase, FollowTrajectoryFactory factory) {
        m_drivebase = drivebase;
        m_factory = factory;
    }
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_factory;

    public Command buildClothoidProgram () {
        Pose2d startPose = new Pose2d(Units.inchesToMeters(20), 2 /*meters*/, Rotation2d.fromDegrees(0)); 
        Spline[] spline = {
            new LinearPseudoSpline(startPose, 2 /*meters*/),
            /*
            new LinearPseudoSpline(InchUtils.translation2d(20.0, 70.0), InchUtils.translation2d(95.3, 84.5)),

            InchUtils.arcPseudoSpline(InchUtils.pose2d(95.3, 84.5, 10.9), 25.0, -0.1),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(107.2, 84.9, -2.9), -25.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(107.2, 84.9), InchUtils.translation2d(201.0, 80.0)),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(201.0, 80.0, -2.9), 20.0, -157.1),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(195.0, 39.4, -177.1), -20.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(195.0, 39.4), InchUtils.translation2d(101.2, 35.0)),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(101.2, 35.0, -177.1), 25.0, -0.1),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(89.3, 36.4, 169.1), -25.0, 12.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(89.3, 36.4), InchUtils.translation2d(20.0, 50.0)),
            */
            
                                                   
        };
        Trajectory traj = HookTrajectories.trajectoryFromSplines(spline, Constants.Drivebase.configForward);
        Command cmd = m_factory.getFollower(traj).beforeStarting(() -> m_drivebase.resetOdometry(startPose));
        return cmd;
    }   
}


