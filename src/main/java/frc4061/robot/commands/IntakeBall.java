package frc4061.robot.commands;

//import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Intake;

public class IntakeBall extends CommandBase{
    private Intake m_intake;

    public IntakeBall(Intake intake) {
        m_intake = intake;
        addRequirements(intake);
    }

    @Override
    public void initialize() {

    };

    @Override
    public void execute() {
        m_intake.extendCollector();
        m_intake.inBalls();

    };

    @Override
    public boolean isFinished() {return false;}

    @Override
    public void end(boolean interrupted)
    {
        //To do: implement a stop intake program
        m_intake.stopIntake();
    }
}
