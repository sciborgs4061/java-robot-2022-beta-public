package frc4061.robot.commands;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.Command;
import java.lang.Runnable;

// This class overcomes the limitation of the andThen() decorator that the 
// andThen(thing) is run as a command. Thus if a command is interrupted
// the andThen(thing) is never run. An andFinally(thing) is run as part of
// ending the decorated command and is always run.

public class CommandsWithFinally extends SequentialCommandGroup {
    Runnable m_finally;
    public CommandsWithFinally(Command... command) {
        super(command);
    }
    public CommandsWithFinally andFinally(Runnable cleanup) {
        m_finally = cleanup;
        return this;
    }
    public void end(boolean interrupted) {
        super.end(interrupted);
        if (m_finally != null) m_finally.run();
    }
}
