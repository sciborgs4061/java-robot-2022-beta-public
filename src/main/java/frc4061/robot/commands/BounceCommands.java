package frc4061.robot.commands;

import frc4061.robot.Constants;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.utilities.InchUtils;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;

public class BounceCommands {

    public BounceCommands(Drivebase drivebase, FollowTrajectoryFactory factory) {
        m_drivebase = drivebase;
        m_factory = factory;
    }
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_factory;

    public Command buildBounceProgram () {
        Pose2d startPose = new Pose2d(Units.inchesToMeters(30), Units.inchesToMeters(94), new Rotation2d()); 
        Spline[] part1 = {
            new LinearPseudoSpline(InchUtils.translation2d(30.0, 94.0), InchUtils.translation2d(48.0, 93.8)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(48.0, 93.8,  0.0), 36.0, 12.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(60.0, 94.5,  9.5), 36.0, 80.5),
            new LinearPseudoSpline(InchUtils.translation2d(90.0, 130.0), InchUtils.translation2d(90.0, 132.0)),
        };

        Trajectory traj1 = HookTrajectories.trajectoryFromSplines(part1, Constants.Drivebase.configForwardNoStop);
        Command cmd1 = m_factory.getFollower(traj1).beforeStarting(() -> m_drivebase.resetOdometry(startPose));
        Spline[] part2 = {
            new LinearPseudoSpline(InchUtils.translation2d(90.0, 132.0), InchUtils.translation2d(90.0, 130.0)),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(90.0, 130.0, -90.0), 30.0,  1.4),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(97.4, 105.6, -64.7), 30.0, 25.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(97.4, 105.6), InchUtils.translation2d(117.0, 64.1)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(117.0, 64.1, -64.7), 29.8, 25.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(130.6, 43.4, -40.7), 29.8, 106.5),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(180.7, 78.3, 89.8), 29.8, 25.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(180.7, 78.3), InchUtils.translation2d(180.0, 132.0)),
        };
        Trajectory traj2 = HookTrajectories.trajectoryFromSplines(part2, Constants.Drivebase.configReversedNoStop);
        Command cmd2 = m_factory.getFollower(traj2).beforeStarting(() -> m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters()));
        Spline[] part3 = {
            new LinearPseudoSpline(InchUtils.translation2d(180.0, 132.0), InchUtils.translation2d(179.6, 86.3)),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(179.6, 86.3, -89.8), 44.8, 25.0, true),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(182.0, 61.5, -73.8), 44.8, 147.7),
            InchUtils.clothoidPseudoSpline(InchUtils.pose2d(270.4, 86.3, 89.8), 44.8, 25.0, false),
            new LinearPseudoSpline(InchUtils.translation2d(270.4, 86.3), InchUtils.translation2d(270.0, 132.0)),
            
        };
        Trajectory traj3 = HookTrajectories.trajectoryFromSplines(part3, Constants.Drivebase.configForwardNoStop);
        Command cmd3 = m_factory.getFollower(traj3).beforeStarting(() -> m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters()));
        Spline[] part4 = {
            new LinearPseudoSpline(InchUtils.translation2d(270.0, 132.0), InchUtils.translation2d(270.0, 120.0)),
            InchUtils.arcPseudoSpline(InchUtils.pose2d(270.0, 120.0, -90.0), 36.0, 83.7),
            new LinearPseudoSpline(InchUtils.translation2d(302.0, 82.2), InchUtils.translation2d(340.0, 78.0)),
            
        };
        Trajectory traj4 = HookTrajectories.trajectoryFromSplines(part4, Constants.Drivebase.configReversedNoStop);
        Command cmd4 = m_factory.getFollower(traj4).beforeStarting(() -> m_drivebase.resetOdometry(m_drivebase.m_odometry.getPoseMeters()));
        
        double totalTime = traj1.getTotalTimeSeconds() + traj2.getTotalTimeSeconds() + traj3.getTotalTimeSeconds() + traj4.getTotalTimeSeconds();
        System.out.println("Bounce predicted time: " + totalTime);

        return (cmd1.andThen(cmd2).andThen(cmd3).andThen(cmd4))
            .beforeStarting(() -> SmartDashboard.putNumber("Auto Timing/Start", Timer.getFPGATimestamp()))
            .andThen(() -> SmartDashboard.putNumber("Auto Timing/Stop", Timer.getFPGATimestamp()));
    }
    
}


