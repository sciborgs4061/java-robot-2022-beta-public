/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc4061.robot;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc4061.robot.commands.BounceCommands;
import frc4061.robot.commands.Characterize;
import frc4061.robot.commands.ClothoidTest;
import frc4061.robot.commands.RunShooter;
import frc4061.robot.commands.SlalomCommands;
import frc4061.robot.Components.BlackBox;
import frc4061.robot.commands.BarrelRaceCommands;
import frc4061.robot.commands.DriveNormal;
import frc4061.robot.commands.FollowTrajectoryFactory;
import frc4061.robot.commands.InterstellarAccuracyCommands;
import frc4061.robot.commands.Playback;
import frc4061.robot.commands.RetractWrist;
import frc4061.robot.commands.Feed;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.subsystems.FalconShooter;
import frc4061.robot.subsystems.Intake;
import frc4061.robot.subsystems.Feeder;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  private static RobotContainer m_robotContainer = new RobotContainer();
  public static RobotContainer getInstance() {
    return m_robotContainer;
  }
  // The robot's subsystems and commands are defined here...
          final Drivebase m_drivebase = new Drivebase();
  private final FalconShooter m_shooter = new FalconShooter();
  private final Intake m_intake = new Intake();
  private final Feeder m_feeder = new Feeder();
  private final Command m_driveNormal = new DriveNormal(m_drivebase);
  private final Command m_runShooter = new RunShooter(m_shooter);
  private final Command m_retractWrist = new RetractWrist(m_intake);
  private final Command m_feed = new Feed(m_feeder);
  private final FollowTrajectoryFactory m_factory = new FollowTrajectoryFactory(m_drivebase);
  private final Command m_iaCmd = new InterstellarAccuracyCommands(m_drivebase, m_factory).buildIAProgram();
  private final Command m_bounceCmd = new BounceCommands(m_drivebase, m_factory).buildBounceProgram();
  private final Command m_barrelRaceCmd = new BarrelRaceCommands(m_drivebase, m_factory).buildBarrelRaceProgram();
  private final Command m_slalomCmd = new SlalomCommands(m_drivebase, m_factory).buildSlalomProgram();
  private final Command m_clothoidCmd = new ClothoidTest(m_drivebase, m_factory).buildClothoidProgram();
  private final Command m_playbackCmd = new Playback(m_drivebase);
  private final SendableChooser<Command> m_chooser = new SendableChooser<>();
  
  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  private RobotContainer() {

     configureButtonBindings();
     m_shooter.setDefaultCommand(m_runShooter);
     m_drivebase.setDefaultCommand(m_driveNormal);
     //m_intake.setDefaultCommand(m_retractWrist);
     m_feeder.setDefaultCommand(m_feed);
     m_chooser.setDefaultOption("Interstellar Accuracy", m_iaCmd);
     m_chooser.addOption("Characterize", new Characterize(m_drivebase));
     m_chooser.addOption("Bounce", m_bounceCmd);
     m_chooser.addOption("Barrel Race", m_barrelRaceCmd);
     m_chooser.addOption("Slalom", m_slalomCmd);
     m_chooser.addOption("Clothoid", m_clothoidCmd);
     // m_chooser.addOption("Playback", m_playbackCmd); 
     SmartDashboard.putData("Auto Command", m_chooser);
     OperatorInterface.initialize(); // ensure that OI entries are actually on SmartDashboard
     BlackBox.instantiate();
     // SmartDashboardUtil.ensureNumber("Driverstation/maxSpeed", Constants.Drivebase.kMaxSpeedMetersPerSecond);
     // SmartDashboardUtil.ensureNumber("Driverstation/maxAccel", Constants.Drivebase.kMaxAccelerationMetersPerSecondSquared);

  }




  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  class DisabledInstantCommand extends InstantCommand {
    DisabledInstantCommand(Runnable r) {
      super(r);
    }
    @Override
    public boolean runsWhenDisabled() {return true;}
  }

   private void configureButtonBindings() {
    // Unused - did not work well enough to be useful - try to resurrect later
    /*
    JoystickButton xButton = new JoystickButton(OperatorInterface.Driver.xboxController, 8); //start button
    xButton.whenPressed(new DisabledInstantCommand(
      () -> BlackBox.getInstance().saveFile()
      )
    );
    */

    // Unused - did not work well enough to be useful
    /*
    JoystickButton forwardButton = new JoystickButton(OperatorInterface.Driverstation.switchbox, 5);
    JoystickButton backwardButton = new JoystickButton(OperatorInterface.Driverstation.switchbox, 6);

    forwardButton.whenPressed (new Forward100(m_drivebase, m_factory).forward100());
    backwardButton.whenPressed (new Back100(m_drivebase, m_factory).back100());
    */
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    return m_chooser.getSelected();
  }
}
