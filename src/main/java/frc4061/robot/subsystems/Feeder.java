package frc4061.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.DMC60;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.OperatorInterface;
import frc4061.robot.RobotMap;

public class Feeder extends SubsystemBase{
    private DMC60 feeder_motor;

    // Will turn these into constants, but leave them as setpoints to test
    double m_intakeSetpoint;
    double m_outputSetpoint;
    
    private static final DMC60 getConfiguredFeederMC(int PWMid)
    {
        var mc = new DMC60(PWMid);
        return mc;
    }

    public Feeder() {
        feeder_motor = getConfiguredFeederMC(RobotMap.FEEDER_MOTOR);
    }


    public void set_speed(double speed){
        if (OperatorInterface.shooterEnabled()) 
        {
            feeder_motor.set(speed);
        }
        else{
            feeder_motor.set(0);
        }
    }

    
    public void stop() {
        set_speed(0);
    }

}
