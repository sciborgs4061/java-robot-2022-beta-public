package frc4061.robot.subsystems;

import com.ctre.phoenix.motorcontrol.StatorCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.motorcontrol.DMC60;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import frc4061.robot.OperatorInterface;
import frc4061.robot.RobotMap;
import frc4061.robot.Constants.*;

public class FalconShooter extends SubsystemBase {
    private TalonFX m_shooterMotorController; 
    //private TalonFX m_lowerMotorController;
    private DMC60 m_pitchMotorController;
    double m_shooterSetpoint;
    //double m_upperSetpoint;
    boolean m_usePID;
    // some constants 
    private static final int kPIDLoopIdx = 0;
    private static final int kTimeoutMs = 30;

    // limit switches
    private DigitalInput pitchLimit;

    // encoder
    private Encoder pitchEncoder;

    //private static final double kF = 1023.0/(0.95* 20660.0); // Freerunning falcon 500 derated 5% for friction (guess)
    //private static final double kP = 0.1; // (all of these are guesses)
    //private static final double kD = 5;
    //private static final double kI = 0.001;
    
    private static final TalonFX getConfiguredMotorController(int canbusID) {
        var mc = new TalonFX(canbusID);
        mc.configFactoryDefault();
        mc.configNeutralDeadband(0.001);
        mc.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, kPIDLoopIdx, kTimeoutMs);
        mc.configNominalOutputForward(0, kTimeoutMs);
        mc.configPeakOutputForward(1, kTimeoutMs);
        mc.configPeakOutputReverse(-1, kTimeoutMs);
        mc.config_kF(kPIDLoopIdx, 0, kTimeoutMs);
        mc.config_kP(kPIDLoopIdx, LowerShooter.kP, kTimeoutMs);
        mc.config_kI(kPIDLoopIdx, LowerShooter.kI, kTimeoutMs);
        mc.config_kD(kPIDLoopIdx, LowerShooter.kD, kTimeoutMs);
        mc.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, LowerShooter.statorLimit, 0, 0));
        return mc;
    }

    private static final DMC60 getConfiguredPitchMotorController(int PWMid) {
        var mc = new DMC60(PWMid);
        return mc;
    }

    public FalconShooter() {
        m_shooterMotorController = getConfiguredMotorController(RobotMap.SHOOTER_CANBUS);
        m_pitchMotorController = getConfiguredPitchMotorController(RobotMap.PITCH_PWM);
        pitchEncoder = new Encoder(RobotMap.PITCH_ENCODER_CHANNEL_A, RobotMap.PITCH_ENCODER_CHANNEL_B, false);
        pitchLimit = new DigitalInput(3);
    }

    public void update() {
        if (OperatorInterface.shooterEnabled()) {
            //if (m_usePID) {
                // RPM to motor-native units of counts/100ms given that there are 
                // 2048 counts/revolution. For upper conversion also multiply
                // by 4/3 for reduction between motor and output shaft.
            var velocityTarget = m_shooterSetpoint  * 2048.0 / 600.0; 
            m_shooterMotorController.set(TalonFXControlMode.Velocity, velocityTarget);
            SmartDashboard.putString("Inputs In Use", "RPM Values");
            //} else {
            //    m_shooterMotorController.set(TalonFXControlMode.PercentOutput, m_shooterSetpoint);
            //    SmartDashboard.putString("Inputs In Use", "Fraction Values");
            //}
        } else {
            m_shooterMotorController.set(TalonFXControlMode.PercentOutput, 0.0);
        }
    }
       
    public void setSpeeds(double shooterRPM) {
        m_usePID = true;
        m_shooterSetpoint = shooterRPM;
     }

    public void setOutputs(double output) {
        m_usePID = false;
        m_shooterSetpoint = output;
    }

    public double getSpeed() {
        var launchSpeed = m_shooterMotorController.getSelectedSensorVelocity();
        // Convert Native Motor units (ticks/100ms) to RPM given 2048 ticks/revolution,
        // Also multiply upper speed by 3/4 for reduction between motor and shaft
        return (launchSpeed  * 600.0/2048.0); 
    }

    public boolean upToSpeed() {
        if (getSpeed() > 0.98 * m_shooterSetpoint)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public double getSupply() {
        double supply = m_shooterMotorController.getSupplyCurrent();

        return supply;
    }

    public double getStator() {
        double stator = m_shooterMotorController.getStatorCurrent();

        return stator;
    }

    public void initializeEncoder() {
        if (pitchLimit.get()){
            pitchEncoder.reset();
        }
    }

    public double getEncoder() {
        return pitchEncoder.getRaw();
    }

    public void stopShooter() {
        setOutputs (0);
    }

    public void liftShooter() {
        m_pitchMotorController.set(OperatorInterface.getLift());
    }

    public void toggleLift(double height) {
        if (OperatorInterface.toggleLift())
        {
            if (!pitchLimit.get())
                zeroShooter();
            moveShooter(height);
        }
    }

    public void autoLiftShooter(double speed) {
        m_pitchMotorController.set(speed);
    }

    public void moveShooter(double finalPos) {
        //zeroShooter();
        while ((finalPos - getEncoder()) < 10)
        {
            autoLiftShooter(0.5);
        }
        while ((getEncoder() - finalPos) < 10)
        {
            autoLiftShooter(-0.2);
        }
        autoLiftShooter(0);
    }

    public void zeroShooter(){
        while (!pitchLimit.get())
        {
            autoLiftShooter(0.35);
        }
        autoLiftShooter(0);
        initializeEncoder();
    }
   
}

