package frc4061.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.DMC60;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.RobotMap;

public class Intake extends SubsystemBase{
    private DMC60 wrist_motor;
    private DMC60 intake_motor;

    private DigitalInput openLimit;
    private DigitalInput closeLimit;

    private static final DMC60 getConfiguredMotorController(int PWMid)
    {
        var mc = new DMC60(PWMid);
        return mc;
    }

    public Intake() {
        wrist_motor = getConfiguredMotorController(RobotMap.UPPER_COLLECTOR_MOTOR);
        intake_motor = getConfiguredMotorController(RobotMap.LOWER_COLLECTOR_MOTOR);
        openLimit = new DigitalInput(RobotMap.COLLECTOR_IN_LIMIT);
        closeLimit = new DigitalInput(RobotMap.COLLECTOR_OUT_LIMIT);
    } 


    public void extendCollector(){
        while (!openLimit.get())
        {
            wrist_motor.set(0.5);
        }
        wrist_motor.disable();

    }
    
    public void retractCollector()
    {
        while (!closeLimit.get())
        {
            wrist_motor.set(0.1);
        }
        wrist_motor.disable();
    }

    public void inBalls(){
        if (!openLimit.get())
        {
            extendCollector();
        }
        intake_motor.set(-1);
    }

    public void stopIntake(){
        retractCollector();
    }

    public boolean retracted() {
        return closeLimit.get();
    }

    public boolean extended() {
        return openLimit.get();
    }
}
