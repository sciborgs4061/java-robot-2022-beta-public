package frc4061.robot.subsystems;

// Imports for SparkMAX motor controllers (NEO)
import com.revrobotics.CANSparkMax;

import frc4061.robot.Components.BlackBox;
import frc4061.robot.Components.CANSparkMaxWithSim;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.REVLibError;
import com.revrobotics.RelativeEncoder;

import frc4061.robot.Constants;
import frc4061.robot.OperatorInterface;
import frc4061.robot.Robot;
import frc4061.robot.RobotMap;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim;
import edu.wpi.first.wpilibj.simulation.SimDeviceSim;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim.KitbotGearing;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim.KitbotMotor;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim.KitbotWheelSize;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.hal.SimDouble;
import edu.wpi.first.hal.simulation.SimDeviceDataJNI;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.SPI;

import java.util.stream.Stream;

public class Drivebase extends SubsystemBase{
  // Driving and sensing
  private final CANSparkMax[] m_leftMotors;
  private final CANSparkMax[] m_rightMotors;
  private final RelativeEncoder[] m_leftEncoders;
  private final RelativeEncoder[] m_rightEncoders;
  public final AHRS m_navX;
  public final DifferentialDrive m_drive;

  // Odometry
  public final DifferentialDriveOdometry m_odometry;
  private final Field2d m_field;

  // Simulation
  private CANEncoderSim[] m_leftEncodersSim;
  private CANEncoderSim[] m_rightEncodersSim;
  private DifferentialDrivetrainSim m_dts;
  private SimDouble m_simAngle;



  public Drivebase() {
    // Motor Controllers
    m_leftMotors = motorIDsToMotors(RobotMap.LEFT_MOTOR_IDS);
    setupFollowers(m_leftMotors);

    m_rightMotors = motorIDsToMotors(RobotMap.RIGHT_MOTOR_IDS);
    setupFollowers(m_rightMotors);
    m_rightMotors[0].setInverted(true);

    m_drive = new DifferentialDrive(m_leftMotors[0], m_rightMotors[0]);
    m_drive.setSafetyEnabled(true);
    m_drive.setExpiration(0.1);
    m_drive.setMaxOutput(1.0);
   
    // Encoders
    m_leftEncoders = motorsToEncoders(m_leftMotors);
    m_rightEncoders = motorsToEncoders(m_rightMotors);

    // Odometry

    m_navX = new AHRS(SPI.Port.kMXP, Constants.Drivebase.kGyroUpdateRateHz);
    m_odometry = new DifferentialDriveOdometry(m_navX.getRotation2d(), new Pose2d(0, 0, new Rotation2d()));
    m_field = new Field2d();
    SmartDashboard.putData("Field", m_field);

    // Simulation - being careful not to instantiate this stuff on a real robot
    if (Robot.isSimulation()) {
      m_leftEncodersSim = motorIDsToCANEncodersSim(RobotMap.LEFT_MOTOR_IDS, false);
      m_rightEncodersSim = motorIDsToCANEncodersSim(RobotMap.RIGHT_MOTOR_IDS, false); 

      var dev = SimDeviceDataJNI.getSimDeviceHandle("navX-Sensor[0]");
      m_simAngle = new SimDouble((SimDeviceDataJNI.getSimValueHandle(dev, "Yaw")));
      m_dts = DifferentialDrivetrainSim.createKitbotSim(KitbotMotor.kDualCIMPerSide, KitbotGearing.k12p75, 
                                                        KitbotWheelSize.kSixInch, null);
    }
  }
    
  public double getLeftEncoderMeters() {
    return _getEncoderMeters(m_leftEncoders);
  }
    
  public double getRightEncoderMeters() {
    return -(_getEncoderMeters(m_rightEncoders)); 
  }
  
  private double _getEncoderMeters( RelativeEncoder[] encoders) {
    double sum = 0.0;
    for (var e : encoders) {
      sum += e.getPosition();
    }
    return sum/encoders.length;
  }

  public double getLeftEncoderMetersPerSec() {
    return getEncoderMetersPerSec(m_leftEncoders);
  }
    
  public double getRightEncoderMetersPerSec() {
    return -getEncoderMetersPerSec(m_rightEncoders); 
  }

  public double getEncoderMetersPerSec(RelativeEncoder[] encoders) {
    double sum = 0.0;
    for (var e : encoders) {
      sum += e.getVelocity();
    }
    return sum/encoders.length; 
  }

  @Override
  public void periodic() {
    m_odometry.update(m_navX.getRotation2d(), getLeftEncoderMeters(), getRightEncoderMeters());
    m_field.setRobotPose(m_odometry.getPoseMeters());
    var left = m_leftMotors[0].get();
    var right = m_rightMotors[0].get();
    var volts = RobotController.getBatteryVoltage();
    if (OperatorInterface.drivebaseEnabled()) {
      BlackBox.getInstance().recordVoltages(left*volts, -right*volts);
    }
  }

  @Override
  public void simulationPeriodic() {
    var voltage = RobotController.getInputVoltage();
    var currHdg = m_dts.getHeading();
    m_dts.setInputs(m_leftMotors[0].get()*voltage,
                    m_rightMotors[0].get()*voltage);
    m_dts.update(Constants.kRobotPeriod);
    var newHdg = m_dts.getHeading();
    var hdgDiff = newHdg.minus(currHdg);
    setEncodersSim(m_leftEncodersSim, m_dts.getLeftPositionMeters()/*+m_leftEncoderBasis*/, m_dts.getLeftVelocityMetersPerSecond());
    setEncodersSim(m_rightEncodersSim, -m_dts.getRightPositionMeters()/*+m_rightEncoderBasis*/, -m_dts.getRightVelocityMetersPerSecond() );
    m_simAngle.set(m_simAngle.get() - hdgDiff.getDegrees());
  }

  public void stop() {
    m_drive.tankDrive(0.0, 0.0);
  }

  public void arcadeDrive(double speed, double turnRate) {
    if (OperatorInterface.drivebaseEnabled()) {
      m_drive.arcadeDrive(speed, turnRate, true); // use squared-inputs setting
    } else {
      stop();
    }
  }

  public void tankDrive(double left, double right) {
    if (OperatorInterface.drivebaseEnabled()) {
      m_drive.tankDrive(left, right, false); 
    } else {
      stop();
    }
  }

  private static final String leftVoltsKey = "Traj Drive Volts/Left";
  private static final String rightVoltsKey = "Traj Drive Volts/Right";

  public void tankDriveVolts(double leftVolts, double rightVolts) {
    if (OperatorInterface.drivebaseEnabled()) {
      /*
      m_leftMotors[0].setVoltage(Math.min(leftVolts, 2));
      m_rightMotors[0].setVoltage(-Math.min(rightVolts, 2));
      */
      double voltage = RobotController.getBatteryVoltage();
      m_drive.tankDrive(Math.min(leftVolts/voltage, 1.0), Math.min(rightVolts/voltage, 1.0), false);
      SmartDashboard.putNumber(leftVoltsKey, leftVolts);
      SmartDashboard.putNumber(rightVoltsKey, rightVolts);
    } else {
      stop();
    }
  }

  public void curvatureDrive(double speed, double turnRadius) {
    if (OperatorInterface.drivebaseEnabled()) {
      if (Math.abs(speed) < Constants.Drivebase.kSpeedDeadband) {
        m_drive.arcadeDrive(speed, turnRadius);
      } else {
      // should we try to calibrate the turn radius to the robot's trackWidth?
      m_drive.curvatureDrive(speed, turnRadius, false);
      }
    } else {
      stop();
    }
  }

  public void resetOdometry(Pose2d pose) {
    resetEncoders();
    m_odometry.resetPosition(pose, m_navX.getRotation2d());
    if (Robot.isSimulation()) m_dts.setPose(pose);
  }

  // setPosition on the This a
  private void resetEncoders() {
    // resetEncoders is private because it should only be done in conjunction
    // with resetting the drive train simulator and odometry poses.
    // That is, use resetOdometry instead.
    for (var e : m_leftEncoders) {
      e.setPosition(0);
    }
    for (var e : m_rightEncoders) {
      e.setPosition(0);
    }
  }
 
  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(getLeftEncoderMetersPerSec(), getRightEncoderMetersPerSec());
  }   

  // Private helper functions
  private static final CANSparkMaxWithSim getConfiguredMotor(int CANBUSID) {
    var motor = new CANSparkMaxWithSim(CANBUSID, MotorType.kBrushless);
    motor.clearFaults();
    motor.restoreFactoryDefaults();
    return motor;
  }

  // Except for all the extra Java verbiage this is just
  // map(CANEncoderSim::new, motorIDs)
  private final CANEncoderSim[] motorIDsToCANEncodersSim(Integer[] motorIDs, boolean invert) {
    return Stream.of(motorIDs).map(m -> new CANEncoderSim(m, invert)).toArray(CANEncoderSim[]::new);
  }

  private final void setEncodersSim (CANEncoderSim[] encodersSim, double position, double velocity) {
    for (var e : encodersSim) {
      e.setPosition(position);
      e.setVelocity(velocity);
    }
  }

  public class CANEncoderSim {
    private final String deviceKey;
    private final String positionKey = "Position";
    private final String velocityKey = "Velocity";
    private final SimDouble positionProp;
    private final SimDouble velocityProp;
    private final SimDeviceSim simSpark;
    private final double sign;
    public CANEncoderSim(int id, boolean invert) {
      deviceKey = "SPARK MAX [" + id + "]";
      simSpark = new SimDeviceSim(deviceKey);
      positionProp = simSpark.getDouble(positionKey);
      velocityProp = simSpark.getDouble(velocityKey);
      sign = invert ? -1.0 : 1.0;
    }
    public void setPosition(double position) {positionProp.set(sign*position);}
    public void setVelocity(double velocity) {velocityProp.set(sign*velocity);}
  }

  private static final RelativeEncoder getConfiguredEncoder(CANSparkMax motor) {
    var enc = motor.getEncoder();
    if (Robot.isReal()) {
      if (enc.setPositionConversionFactor(Constants.Drivebase.kDistancePerEncoderPulse) != REVLibError.kOk) {
        throw new Invalid("Invalid Position Conversion Factor");
      }
      if (enc.setVelocityConversionFactor(Constants.Drivebase.kDistancePerEncoderPulse/60) != REVLibError.kOk) {
        throw new Invalid("Invalid Position Conversion Factor");
      }
    }
    return enc;
  }
  // Except for all the extra Java verbiage this is just
  // map(m -> m.getEncoder(), motors)
  private static final RelativeEncoder[] motorsToEncoders(CANSparkMax[] motors) {
    return Stream.of(motors).map(m -> getConfiguredEncoder(m)).toArray(RelativeEncoder[]::new);
  }

  private static final void setupFollowers(CANSparkMax[] motors) {
    motors[1].follow(motors[0]);
    motors[2].follow(motors[0]);
  }

  // Except for all the extra Java verbiage this is just
  // map(CANEncoderSim::new, motorIDs)
  private static final CANSparkMaxWithSim[] motorIDsToMotors(Integer[] motorIDs) {
    return Stream.of(motorIDs).map(id -> getConfiguredMotor(id)).toArray(CANSparkMaxWithSim[]::new);
  }

  private static class Invalid extends RuntimeException {
    public Invalid(String errorMessage) {super(errorMessage);}
  }

}
