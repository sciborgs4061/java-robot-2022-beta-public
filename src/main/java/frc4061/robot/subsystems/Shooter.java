package frc4061.robot.subsystems;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.motorcontrol.PWMTalonFX;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.simulation.EncoderSim;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.math.Pair;
import frc4061.robot.Constants.*;
import frc4061.robot.RobotMap;

public class Shooter extends SubsystemBase {
    PWMTalonFX m_upperMotorController; 
    PWMTalonFX m_lowerMotorController;
    Encoder m_upperEncoder;
    Encoder m_lowerEncoder;
    PIDController m_upperPID;
    PIDController m_lowerPID; 
    double m_upperMotorVoltage;
    double m_lowerMotorVoltage;
    // Simulator support
    EncoderSim m_upperEncoderSim;
    EncoderSim m_lowerEncoderSim;
    FlywheelSim m_upperMotorSim;
    FlywheelSim m_lowerMotorSim;

    public Shooter() {
        // m_upperMotorController = new TalonFX(RobotMap.SHOOTER_UPPER_CANBUS);
        // m_lowerMotorController = new TalonFX(RobotMap.SHOOTER_LOWER_CANBUS);
        m_upperMotorController = new PWMTalonFX(0);
        //m_lowerMotorController = new PWMTalonFX(1);
        m_upperEncoder = new Encoder(RobotMap.PITCH_ENCODER_CHANNEL_A, RobotMap.PITCH_ENCODER_CHANNEL_B, false);
        // m_upperEncoder.setDistancePerPulse(1.0/(4096)); // output RPM
        //m_lowerEncoder = new Encoder(RobotMap.LOWER_SHOOTER_ENCODER_BASE, RobotMap.LOWER_SHOOTER_ENCODER_BASE+1, false);
        // m_lowerEncoder.setDistancePerPulse(1.0/(4096));
        m_upperPID = new PIDController(UpperShooter.kP, UpperShooter.kI, 0);
        m_upperPID.setTolerance(2);
        m_upperPID.setIntegratorRange(-200, 200);
        //m_lowerPID = new PIDController(LowerShooter.kP, LowerShooter.kI, 0);
        //m_lowerPID.setTolerance(2);
        //m_lowerPID.setIntegratorRange(-200, 200);
        m_upperEncoderSim = new EncoderSim(m_upperEncoder);
        //m_lowerEncoderSim = new EncoderSim(m_lowerEncoder);
        m_upperMotorSim = new FlywheelSim(DCMotor.getFalcon500(1), UpperShooter.kReduction, UpperShooter.kMI);
        //m_lowerMotorSim = new FlywheelSim(DCMotor.getFalcon500(1), LowerShooter.kReduction, LowerShooter.kMI);
    }

    public void update() {
        var speeds = getSpeeds();
        var upperMotorFeedforward = m_upperPID.getSetpoint();
        if (upperMotorFeedforward == 0) {
            m_upperMotorController.setVoltage(0);
        } else {
            var upperMotorFeedback = m_upperPID.calculate(speeds.getFirst()); // in units of "configured kVoltsPerRPM RPMs"
            var upperMotorCandidate = upperMotorFeedforward+upperMotorFeedback;
            var upperMotorLimit = speeds.getFirst()+UpperShooter.kMaxStep;
            m_upperMotorVoltage = Math.min(upperMotorCandidate, upperMotorLimit)*UpperShooter.kVoltPerRPM;
            m_upperMotorController.setVoltage(m_upperMotorVoltage);
        }
    
        //var lowerMotorFeedforward = m_lowerPID.getSetpoint();
        //if (lowerMotorFeedforward == 0) {
            //m_lowerMotorController.setVoltage(0);
        //} else {
            //var lowerMotorFeedback = m_lowerPID.calculate(speeds.getSecond());
            //var lowerMotorCandidate = lowerMotorFeedforward+lowerMotorFeedback;
            //var lowerMotorLimit = speeds.getSecond()+LowerShooter.kMaxStep;
            //m_lowerMotorVoltage = Math.min(lowerMotorCandidate, lowerMotorLimit)*LowerShooter.kVoltPerRPM;
            //m_lowerMotorController.setVoltage(m_lowerMotorVoltage);
        //}
    }
       
    public void setSpeeds(double upperRPM, double lowerRPM) {
        m_upperPID.setSetpoint(upperRPM);
        m_lowerPID.setSetpoint(lowerRPM);

    }

    public void setMotorOutputs(double upperVoltage, double lowerVoltage) {
        m_upperMotorVoltage = upperVoltage;
        m_lowerMotorVoltage = lowerVoltage;
    }

    public Pair<Double, Double> getSpeeds() {
        return Pair.of(m_upperEncoder.getRate(), m_lowerEncoder.getRate());
    }

    public void setGains(double upperP, double upperI, double lowerP, double lowerI) {
        m_upperPID.setPID(upperP, upperI, 0);
        m_lowerPID.setPID(upperP, upperI, 0);
    }

    public void stopShooter() {
        // set motor speeds to 0
    }

    public void simulationPeriodic() {
        m_upperMotorSim.setInputVoltage(m_upperMotorController.get()*RobotController.getInputVoltage());
        m_upperMotorSim.update(0.02);
        m_lowerMotorSim.setInputVoltage(m_lowerMotorController.get()*RobotController.getInputVoltage());
        m_lowerMotorSim.update(0.02);
        m_upperEncoderSim.setRate(m_upperMotorSim.getAngularVelocityRPM());
        m_lowerEncoderSim.setRate(m_lowerMotorSim.getAngularVelocityRPM());
        SmartDashboard.putNumber("Upper Motor Current", m_upperMotorSim.getCurrentDrawAmps());
        SmartDashboard.putNumber("Lower Motor Current", m_lowerMotorSim.getCurrentDrawAmps());
    }
    
}

