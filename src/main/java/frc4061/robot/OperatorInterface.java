/*******************************************************************
 * Operator Interface Devices and Access Methods
 */

package frc4061.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.Joystick;

public final class OperatorInterface {

    // Public methods for accessing OperatorInterface devices
    public static final boolean drivebaseEnabled() { return Driverstation.drivebaseEnabled(); }
    public static final boolean shooterEnabled() { return Driverstation.shooterEnabled(); }
    public static final boolean collectorEnabled() { return Driverstation.collectorEnabled(); }
    public static final double  getSpeed() { return Driver.getSpeed(); }
    public static final double  getTurn()  { return Driver.getTurn(); }
    public static final boolean getProceed() {return Driver.getProceed(); }
    private static final boolean getTurbo() {return Driver.getTurbo(); }
    private static final boolean getAntiTurbo() {return Driver.getAntiTurbo(); }
    public static final double getFeeding() {return Driver.getFeeding();}
    public static final double getLift() {return Operator.getLift();}
    public static final boolean toggleLift() {return Operator.toggleLift();}
    public static final boolean incrementShooterByTen() {return Driverstation.incrementShooterByTen();}
    public static final boolean decrementShooterByTen() {return Driverstation.decrementShooterByTen();}
    public static final void initialize() {
        // ensure that Smartdashboard entries are instantiated
        Driver.initialize();
        Operator.initialize();
        Driverstation.initialize();
    }
    public static final void update() {
        // ensure that Smartdashboard entries are instantiated
        Driver.update();
        Operator.update();
        Driverstation.update();
    }
    

    // Driver devices
    static class Driver {
        private static final int driverGameControllerId = 0;
        private static final int speedAxis = 1;
        private static final int turnAxis = 4;
        private static final int turboAxis = 3;
        private static final int antiTurboAxis = 2;
        public static final XboxController xboxController = new XboxController(driverGameControllerId);
        private static final double getSpeed() { 
            if (getAntiTurbo())
                return (-xboxController.getRawAxis(speedAxis) * 0.60) * (1 - xboxController.getRawAxis(antiTurboAxis)*0.5);
            else if (getTurbo())
                return (-xboxController.getRawAxis(speedAxis) * 0.60) * (1 + xboxController.getRawAxis(turboAxis)*0.5);
            else
                return -xboxController.getRawAxis(speedAxis) * 0.60;
        }
        private static final double getTurn()  { 
            if (getAntiTurbo())
                return xboxController.getRawAxis(turnAxis)*0.25;
            else if (getTurbo())
                return xboxController.getRawAxis(turnAxis)*0.50;
            else
                return xboxController.getRawAxis(turnAxis)*0.50;
        }
        private static final boolean getTurbo() {return (xboxController.getRawAxis(turboAxis) > 0.5); }
        private static final boolean getAntiTurbo() {return (xboxController.getRawAxis(antiTurboAxis) > 0.5); }
        private static final boolean getProceed() {return xboxController.getLeftBumper() && xboxController.getRightBumper(); }
        private static final double getFeeding() { 
            if (xboxController.getAButton()){
                return -0.55;
            }
            else if (xboxController.getBButton()){
                return -0.25;
            }
            else
            {
                return 0;
            }
        }
        private static final void initialize() {}
        private static final void update() {}
    }

    // Operator devices
    static class Operator {
        // nothing interesting here yet
        private static final int operatorGameControllerId = 1;
        private static final void initialize() {}
        private static final void update() {}
        
        private static final XboxController secondController = new XboxController(operatorGameControllerId);

        private static final double getLift() {
            return secondController.getRawAxis(1);
        }

        private static final boolean toggleLift() {
            return secondController.getBButton();
        }
    }

    // Driverstation devices
    static class Driverstation {  
         
        private static final int driverstationJoystickId = 2; // actually, just a switchbox


        public static final Joystick switchbox = new Joystick(driverstationJoystickId);

        // Drivebase enable logic: hard switch + override on SmartDashboard
        private static final int drivebaseEnableSwitchId = 12; // button/switch indexes begin with 1
        private static final int collectorEnableSwitchId = 2; // button/switch indexes begin with 1
        private static final int incrementShooter = 3;
        private static final int decrementShooter = 4;
        private static String driverstationKey = "Driverstation/";
        private static String collectorKey = "Collector/";
        private static String drivebaseOverrideKey = driverstationKey + "Drivebase Override";
        private static String drivebaseEnabledKey = driverstationKey + "Drivebase Enabled";
        private static String collectorEnabledKey = collectorKey + "Collector Enabled";
        private static boolean drivebaseOverrideDefault = false; 
        private static boolean m_drivebaseEnabled = false;
        private static boolean m_collectorEnabled = false;
        private static final boolean drivebaseOverride() {
            return SmartDashboard.getBoolean(drivebaseOverrideKey, false);
        }
        private static final boolean drivebaseEnabledSwitch() { return switchbox.getRawButton(drivebaseEnableSwitchId); }
        private static final boolean drivebaseEnabled() { return m_drivebaseEnabled; }

        private static final boolean collectorEnabledSwitch() { return switchbox.getRawButton(collectorEnableSwitchId); }
        private static final boolean collectorEnabled() { return m_collectorEnabled; }
        // Shooter enable logic: hard switch + override on SmartDashboard
        private static final int shooterEnableSwitchId = 2;
        private static String shooterOverrideKey = driverstationKey + "Shooter Override";
        private static String shooterEnabledKey = driverstationKey + "Shooter Enabled";
        private static boolean shooterOverrideDefault = false; 
        private static boolean m_shooterEnabled = false;
        private static final boolean shooterOverride() {
            return SmartDashboard.getBoolean(shooterOverrideKey, false);
        }
        private static final boolean shooterEnabledSwitch() { return switchbox.getRawButton(shooterEnableSwitchId); }
        private static final boolean shooterEnabled() { return m_shooterEnabled;}
        private static final boolean decrementShooterByTen() {return switchbox.getRawButton(decrementShooter);}
        private static final boolean incrementShooterByTen() {return switchbox.getRawButton(incrementShooter);}
        private static final void initialize() {
            SmartDashboard.putBoolean(drivebaseOverrideKey, drivebaseOverrideDefault);
            SmartDashboard.putBoolean(shooterOverrideKey, shooterOverrideDefault);
            SmartDashboard.putBoolean(shooterEnabledKey, shooterOverrideDefault);
            SmartDashboard.putBoolean(drivebaseEnabledKey, drivebaseOverrideDefault);
        } 
        private static final void update() {
            m_drivebaseEnabled  =  drivebaseOverride() || drivebaseEnabledSwitch();
            SmartDashboard.putBoolean(drivebaseEnabledKey, m_drivebaseEnabled);
            m_shooterEnabled = shooterOverride() || shooterEnabledSwitch();
            SmartDashboard.putBoolean(shooterEnabledKey, m_collectorEnabled);
            m_collectorEnabled = collectorEnabledSwitch();
            SmartDashboard.putBoolean(collectorEnabledKey, m_collectorEnabled);

        }
    }


}
