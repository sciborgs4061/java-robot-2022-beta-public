/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc4061.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj.Timer;
import frc4061.robot.Components.BlackBox;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private static final String leftEncoders = "Encoders/Left";
  private static final String rightEncoders = "Encoders/Right";
  private static final String leftEncoderMPS = "Encoders/Left m per s";
  private static final String rightEncoderMPS = "Encoders/Right m per s";
  private Command m_autonomousCommand;

  private RobotContainer m_robotContainer;
    
  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    // Instantiate our RobotContainer.  This will perform all our button bindings, and put our
    // autonomous chooser on the dashboard.
    m_robotContainer = RobotContainer.getInstance();
    SmartDashboard.putNumber(leftEncoders, 0);
    SmartDashboard.putNumber(rightEncoders, 0);
    SmartDashboard.putNumber(leftEncoderMPS, 0);
    SmartDashboard.putNumber(rightEncoderMPS, 0);
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    // Runs the Scheduler.  This is responsible for polling buttons, adding newly-scheduled
    // commands, running already-scheduled commands, removing finished or interrupted commands,
    // and running subsystem periodic() methods.  This must be called from the robot's periodic
    // block in order for anything in the Command-based framework to work.
    OperatorInterface.update();
    double leftEncodersCounts = m_robotContainer.m_drivebase.getLeftEncoderMeters();
    double rightEncodersCounts = m_robotContainer.m_drivebase.getRightEncoderMeters();

    double leftMPS = m_robotContainer.m_drivebase.getLeftEncoderMetersPerSec();
    double rightMPS =m_robotContainer.m_drivebase.getRightEncoderMetersPerSec();

    SmartDashboard.putNumber(leftEncoders, leftEncodersCounts);
    SmartDashboard.putNumber(rightEncoders, rightEncodersCounts);

    SmartDashboard.putNumber(leftEncoderMPS, leftMPS);
    SmartDashboard.putNumber(rightEncoderMPS, rightMPS);

    Constants.Drivebase.kMaxSpeedMetersPerSecond = SmartDashboard.getNumber("Driverstation/maxSpeed", Constants.Drivebase.kMaxSpeedMetersPerSecond);
    Constants.Drivebase.kMaxAccelerationMetersPerSecondSquared = SmartDashboard.getNumber("Driverstation/maxAccel", Constants.Drivebase.kMaxAccelerationMetersPerSecondSquared);
    CommandScheduler.getInstance().run();
  }

  /**
   * This function is called once each time the robot enters Disabled mode.
   */
  @Override
  public void disabledInit() {
    SmartDashboard.putString("RobotState", "Disabled");
  }

  @Override
  public void disabledPeriodic() {
  }

  /**
   * This autonomous runs the autonomous command selected by your {@link RobotContainer} class.
   */
  @Override
  public void autonomousInit() {
    SmartDashboard.putString("RobotState", "Autonomous");
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    // schedule the autonomous command (example)
    if (m_autonomousCommand != null) {
      Timer.delay(0.050);
      m_autonomousCommand.schedule();
    }
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
  }

  @Override
  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    SmartDashboard.putString("RobotState", "Teleop");
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
    BlackBox.instantiate();
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
  }

  @Override
  public void testInit() {
    SmartDashboard.putString("RobotState", "Test");
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
