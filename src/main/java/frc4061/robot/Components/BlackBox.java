package frc4061.robot.Components;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;

public class BlackBox {
    double data[][];
    int writeIndex = 0;
    private static final int dataSize = 10000;
    private static BlackBox instance;

    private BlackBox() {
        data = new double[dataSize][2];
        writeIndex = 0;
    }

    public static void instantiate() {
        instance = new BlackBox();
    }

    public static BlackBox getInstance() {
        return instance;
    }

    public void recordVoltages(double left, double right) {
        if ((left != 0) || (right != 0) || (writeIndex != 0)) {
            instance.data[writeIndex][0] = left;
            instance.data[writeIndex][1] = right;
            writeIndex = writeIndex < dataSize - 2 ? writeIndex + 1 : writeIndex;
        }
    }

    public void saveFile() {
        /* since we are not using playback, disable saving to avoid filling filesystem
        var timestamp = ZonedDateTime.now(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofPattern("uuuu.MM.dd.HH.mm.ss"));
        DataOutputStream os;
        Path linkName = Paths.get("/", "home", "lvuser", "BlackBoxData-playback");
        Path blackBoxFile = Paths.get("/", "home", "lvuser", "BlackBoxData-" + timestamp);

        try {
            os = new DataOutputStream(new FileOutputStream("/home/lvuser/BlackBoxData-" + timestamp));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            os = null;
        }
        try {
            for (int i = 0; i < writeIndex; i++) {
                os.writeDouble(data[i][0]);
                os.writeDouble(data[i][1]);
            } 
            os.flush();
            os.close();
            if (Files.exists(linkName)) {Files.delete(linkName);}
            Files.createSymbolicLink(linkName, blackBoxFile);
        } catch (IOException e) {
             e.printStackTrace();
        }
        */
    }


}
