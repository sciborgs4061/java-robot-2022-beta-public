package frc4061.robot.Components;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.wpilibj.RobotController;
import frc4061.robot.Robot;

public class CANSparkMaxWithSim extends CANSparkMax {
    private double m_setPoint;

    public CANSparkMaxWithSim(int deviceID, MotorType type) {
		super(deviceID, type);
    }

    public double get() {
        if (Robot.isSimulation()) return m_setPoint;
        else return super.get();
    }

    public void set(double speed) {
        if (Robot.isSimulation()) m_setPoint = speed;
        super.set(speed);
    }

    public void setVoltage(double voltage) {
        if (Robot.isSimulation()) m_setPoint = voltage/RobotController.getBatteryVoltage();
        super.setVoltage(voltage);
    } 
}
