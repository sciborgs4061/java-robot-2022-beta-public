package frc4061.robot.Components;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SmartDashboardUtil {
    public static final void ensureNumber(String key, double defaultVal) {
        SmartDashboard.putNumber(
            key,
            SmartDashboard.getNumber(key, defaultVal)
        );
    }
    public static final void ensureBoolean(String key, boolean defaultVal) {
        SmartDashboard.putBoolean(
            key,
            SmartDashboard.getBoolean(key, defaultVal)
        );
    }  
}
