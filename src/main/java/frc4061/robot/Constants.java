/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc4061.robot;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final double kRobotPeriod = 0.02;
    public final static class Drivebase {
        public static final double kRobotLengthInches = 38;
        public static final double kSpeedDeadband = 0.10;
        public static final double kTurnDeadband = 0.05;
        // The NEO encoders report in units of motor revolutions; in the code below this is considered a "pulse"
        // even though the hardware encoders have 42 pulses per revolution
        public static final double kDistancePerEncoderPulse = (((3.14 * 6.0)/39.37)/(9.45))/1.1805;
        //  (((pi*6 inches)/(39.37 inches/meter))/outputRevolution) / ((1 pulse/inputRevolution)*(17.857 inputRevolution/outputRevolution)
        //  / (empirically measured factor)
        public static final byte kGyroUpdateRateHz = 50; // matches robot period of 20ms

        // constants from frc-characterization for the simulated autotest robot
        // test run on 3/30/21.
        /*
        public static final double ksVolts = 0.00306;
        public static final double kvVoltSecondsPerMeter = 3.54;
        public static final double kaVoltSecondsSquaredPerMeter = 0.02;
        public static final double rSquared = 1.0;
        public static final double kTrackWidthMeters = 0.664;
        public static final double kPDriveVel = 0.105;
        public static final double kD = 0;
        */
        // Robot constants from characterization run on 3/31/21
        
        public static final double ksVolts = 0.164; 
        public static final double kvVoltSecondsPerMeter = 2.94;
        public static final double kaVoltSecondsSquaredPerMeter = 0.413;
        public static final double rSquared = 1.0;
        public static final double kTrackWidthMeters = 0.572;
        public static final double kPDriveVel = 0.2; // halve this if too jerky; halve again if still too jerky
        public static final double kD = 0; // calculated 17.8
        
        


        // instances encapsulating the above
        public static final DifferentialDriveKinematics kDriveKinematics =
            new DifferentialDriveKinematics(kTrackWidthMeters);
        public static double kMaxSpeedMetersPerSecond = 2.60;
        public static double kMaxAccelerationMetersPerSecondSquared = 1.45;
        // best settings for barrel - speed 2.35, accel 1.5, kP = 0.105
        // best settings for bounce - speed 2.75, accel 2.0, dP = 0.105
        // best so far for slalom: 2.35, accel 1.25, kP 0.105

        // RAMSETE follower parameters - not robot specific
        public static final double kRamseteB = 2;
        public static final double kRamseteZeta = 0.7;

        // Constraints and configs related to drivebase constants
        public static final DifferentialDriveVoltageConstraint autoVoltageConstraint =
            new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(ksVolts,
                                           kvVoltSecondsPerMeter,
                                           kaVoltSecondsSquaredPerMeter),
                kDriveKinematics, 10);
        public static final DifferentialDriveVoltageConstraint highVoltageConstraint =
            new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(ksVolts,
                                            kvVoltSecondsPerMeter,
                                            kaVoltSecondsSquaredPerMeter),
                kDriveKinematics, 12);               
        public static final TrajectoryConfig configForward =
            new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                                 kMaxAccelerationMetersPerSecondSquared)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(autoVoltageConstraint);
        public static final TrajectoryConfig configForwardNoStop =
            new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                                    kMaxAccelerationMetersPerSecondSquared)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(autoVoltageConstraint)
                .setEndVelocity(1.9);  
        public static final TrajectoryConfig configFastForward =
            new TrajectoryConfig(3.6,
                                 4)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(highVoltageConstraint)
                .setStartVelocity(1.9);    
        public static final TrajectoryConfig configStraightForward =
            new TrajectoryConfig(3.6,
                                    4)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(highVoltageConstraint);
        public static final TrajectoryConfig configStraightReversed =
            new TrajectoryConfig(3.6,
                                    4)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(highVoltageConstraint)
                .setReversed(true);
    
        public static final TrajectoryConfig configReversed = 
            new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                                 kMaxAccelerationMetersPerSecondSquared)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(Constants.Drivebase.kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(autoVoltageConstraint)
                .setReversed(true);
        public static final TrajectoryConfig configReversedNoStop =
            new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                                    kMaxAccelerationMetersPerSecondSquared)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(autoVoltageConstraint)
                .setEndVelocity(1.9)
                .setReversed(true); 
    }
    public final static class UpperShooter {
        public static final double  kReduction = 4.0/3.0; // input turns over output turns
        public static final double kMI = 0.00005; // moment of inertia - kg M^2
        public static final double kVoltPerRPM = (2.5/1000); // correct value; RPM's of the output shaft
        // public static final double kVoltPerRPM = 2.0/1000; // experiment for robustness
        public static final double kP = 0.09; 
        public static final double kI = 0.0005;
        public static final double kD = 0;
        public static final double kMaxStep = 400; // max amount new commanded output can exceed current speed
        // turns CW looking at the shaft end (opposite direction from lower motor)
        public static final double statorLimit = 35;
    }
    public final static class LowerShooter {
        public static final double kReduction = 1.0; // input turns over output turns
        public static final double kMI = 0.0033; // moment of inertia - kg M^2
        public static final double kVoltPerRPM = (1.87/1000); // correct value
        // public static final double kVoltPerRPM = 2.25/1000; // experiment for robustness
        public static final double kP = 0.25; // make up half the difference 
        public static final double kI = 0.000015;
        public static final double kD = 0.0000002;
        public static final double kMaxStep = 250; // max amount new commanded output can exceed current speed
        // turns CCW looking at the shaft end
        public static final double statorLimit = 35;
    }

}